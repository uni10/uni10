#!/usr/bin/env python3
"""
Description:
This program provides the functionality to convert the tensor saved in HDF5 format in uni10 v1.0.0 into the same file which can be read from uni10 v2.0.0.

Disclaimer:
There is no guarantee that this program will convert it successfully. Therefore, backup is highly recommended before executing this program.

Author:
Chen-Yen Lai

Date:
June 11th 2018
"""
import numpy as np
import h5py
import argparse

def ConvertH5(args):
    Filename = args["input"]
    GroupName = args["group"]
    f = h5py.File(Filename, "a")
    g = f[GroupName]
    ArrDim = g["Block"]["m_elem"].shape[0]
    g.create_dataset("Block/ElemNum", data=ArrDim)
    g["Block"]["U_elem"] = g["Block"]["m_elem"]
    del g["Block"]["m_elem"]
    g["Bonds"]["BondNum"] = g["Bonds"]["bondNum"]
    del g["Bonds"]["bondNum"]
    g["Bonds"]["Labels"] = g["Bonds"]["labels"]
    del g["Bonds"]["labels"]
    BondNum = g["Bonds"]["BondNum"][()]
    for i in range(BondNum):
        g["Bonds"]["bond-"+str(i)]["NumQnum"] = g["Bonds"]["bond-"+str(i)]["numQnums"]
        del g["Bonds"]["bond-"+str(i)]["numQnums"]
        g["Bonds"]["bond-"+str(i)]["BondType"] = g["Bonds"]["bond-"+str(i)]["bondType"]
        del g["Bonds"]["bond-"+str(i)]["bondType"]
        g["Bonds"]["bond-"+str(i)]["Qdegs"] = g["Bonds"]["bond-"+str(i)]["degs"]
        del g["Bonds"]["bond-"+str(i)]["degs"]
    f.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog="h5convert.py", usage='python %(prog)s [options]', description="Convert tensor saved in HDF5 format from v1.0.0 to v2.0.0.", epilog="Please backup your original file before converting.")
    parser.add_argument("-in", "--input", metavar="input-file.h5", type=str, help="Filename of input file including suffix")
    parser.add_argument("-g", "--group", metavar="group/subgroup", type=str, help="Group name of the tensor")
    args = vars(parser.parse_args())

    ConvertH5(args)
