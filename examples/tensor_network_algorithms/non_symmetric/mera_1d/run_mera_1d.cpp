#include "../../../hamiltonian/hamiltonian.h"
#include "mera_tools/mera_1d.h"

using namespace std;
using namespace uni10;

int main(){

  Uni10Create();
  Uni10PrintEnvInfo();

  mera_paras paras;
  paras.load_mera_paras();

  UniTensor<uni10_double64> hamiltonian_d;
  UniTensor<uni10_complex128> hamiltonian_c;

  bool is_real = load_hamiltonian(hamiltonian_d, hamiltonian_c);

  Network ascL("./Diagrams/AscendL") ; Network ascC("./Diagrams/AscendC") ; Network ascR("./Diagrams/AscendR");
  Network desL("./Diagrams/DescendL"); Network desC("./Diagrams/DescendL"); Network desR("./Diagrams/DescendR");
  Network WLEnvL("./Diagrams/WLEnvL"); Network WLEnvC("./Diagrams/WLEnvC"); Network WLEnvR("./Diagrams/WLEnvR");
  Network WREnvL("./Diagrams/WREnvL"); Network WREnvC("./Diagrams/WREnvC"); Network WREnvR("./Diagrams/WREnvR");
  Network UEnvL("./Diagrams/UEnvL")  ; Network UEnvC("./Diagrams/UEnvC")  ; Network UEnvR("./Diagrams/UEnvR");

  map<string, Network*> net_list;
  net_list["ascL"] = &ascL; net_list["ascC"] = &ascC; net_list["ascR"] = &ascR;
  net_list["desL"] = &desL; net_list["desC"] = &desC; net_list["desR"] = &desR;
  net_list["WLEnvL"] = &WLEnvL; net_list["WLEnvC"] = &WLEnvC; net_list["WLEnvR"] = &WLEnvR;
  net_list["WREnvL"] = &WREnvL; net_list["WREnvC"] = &WREnvC; net_list["WREnvR"] = &WREnvR;
  net_list["UEnvL"] = &UEnvL; net_list["UEnvC"] = &UEnvC; net_list["UEnvR"] = &UEnvR;

  if(is_real){

    MERA_1D<uni10_double64> mera_run(hamiltonian_d, paras, net_list);
    mera_run.Optimize();

  }
  else{
    MERA_1D<uni10_complex128> mera_run(hamiltonian_c, paras, net_list);
    mera_run.Optimize();
  }

  Uni10Destroy();

  return 0;
}
