###
#  @file CMakeLists.txt
#  @license
#    Copyright (c) 2013-2017
#    National Taiwan University
#    National Tsing-Hua University
#
#    This file is part of Uni10.
#
#    Uni10 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Uni10 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Uni10.  If not, see <http://www.gnu.org/licenses/>.
#  @endlicense
#  @brief Main specification file for CMake
#  @author Ying-Jer Kao
#  @date 2014-05-06
#  @since 0.1.0
###



######################################################################
### BUILD SHARED LIBRARY
######################################################################
include_directories(${CMAKE_SOURCE_DIR}/include)
set(idmrg_1d_sources
   ../../../operator/operator.cpp
   ../../../hamiltonian/hamiltonian.cpp
   ../../../mpo_hamiltonian/mpo_hamiltonian.cpp
   idmrg_tools/idmrg_tools.cpp
   idmrg_tools/mpsinf.cpp
   ../../../common/common_tools.cpp
   run_idmrg_1d
)
add_executable(run_idmrg_1d ${idmrg_1d_sources})
target_link_libraries(run_idmrg_1d  ${LAPACK_LIBRIARIES} uni10-static)
######################################################################
### BUILD EXAMPLES
######################################################################
install(TARGETS run_idmrg_1d DESTINATION examples/bin/idmrg_1d COMPONENT examples)
install(FILES .uni10rc .hamrc .idmrgrc DESTINATION examples/bin/idmrg_1d COMPONENT examples)

