#ifndef __COMMON_TOOLS_H__
#define __COMMON_TOOLS_H__

#include <stdio.h>

void progressbar(long long int pos, long long int begin, long long int end, bool isflush = false);

#endif
