# 
# --------------------
# version name of uni10

## Name of the package of the linear algebra solvers.
LNPACKAGE   = lapack
## Computational architecture.
CALARCH     = cpu

# --------------------
#
# --------------------
# programs

CXX       = g++

ARCH      = ar
ARCHFLAGS = cr
RANLIB    = ranlib

# --------------------
#
# --------------------
# flags

# UNI10 MACRO FLAGS
UNI10CXXFLAGS = -DUNI_CPU -DUNI_LAPACK -DUNI_TESTING -DUNI_TCL

# Use -fPIC to make shared (.so) and static (.a) library;
# can be commented out if making only static library.
##
## Our ATLAS installation has only static libraries, and one can't
## build a shared library against a static library, so disable FPIC.
FPIC          = -fPIC
CFLAGS        = -Wall -O3 $(FPIC) -fopenmp -Wno-unused-function -Wno-unused-variable
LDFLAGS       =     $(FPIC) -fopenmp
STATICFLAGS   = -Wl,--whole-archive 

# USE BOOST random generators or not.
#
# CFLAGS    += -DBOOST
#

# C++11 (gcc >= 4.7) is not required, but has benefits like atomic operations
CXXFLAGS  := $(UNI10CXXFLAGS) $(CFLAGS) -pthread -fPIC -fpermissive -std=c++11
CFLAGS    += -std=c99

WARNINGS  = -Wno-uninitialized

# --------------------
#
# --------------------
# link librarys

LDLIBRARIES := stdc++ m

# USE BOOST random generators or not.
#
# LDLIBRARIES    += boost_system boost_filesystem boost_thread
#
# BLAS/LAPACK links for Ubuntu
#  
LDLIBRARIES += blas lapack

# Compiler options

CXXFLAGS +=

# BLAS/LAPACK include

BLAS_INCLUDE := 
BLAS_LIB     :=

# --------------------
#
# --------------------
# Use HPTT/TCL

HPTT_INCLUDE := $(HPTT_ROOT)/include
HPTT_LIB     := $(HPTT_ROOT)/lib
TCL_INCLUDE  := $(TCL_ROOT)/include
TCL_LIB      := $(TCL_ROOT)/lib

LDLIBRARIES += hptt tcl

# --------------------
#
# --------------------
# customized settings

UNI10_INSTALL_PREFIX := /home/yunhsuan/myLib/uni10_cpu

# --------------------
#
# --------------------
# google test

GTEST := 0

# Uncomment for debugging. Does not work on OSX due to https://github.com/BVLC/caffe/issues/171
# DEBUG := 1

# --------------------

# enable pretty build (comment to see full commands)
Q ?= @
