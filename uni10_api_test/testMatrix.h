#ifndef _TESTMATRIX_H_
#define _TESTMATRIX_H_

#include "uni10.hpp"

// Define some size of matrix to be used
struct size {
  size_t row;
  size_t col;
};

enum shape {
  TS, SS, WS,
  TM, SM, WM,
  TL, SL, WL,
};

struct size sizes[] = {{8,3}, {5,5}, {3,8}};
//                       {37,17}, {25,25}, {17,37},
//                       {178,88}, {125,125}, {88,178}};

double error = 1e-6;

#endif
