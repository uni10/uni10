# Uni10 APP Development Guide

In Uni10 v2.0, we have made substantial changes to accommodate different
architectures on which uni10 can run. Now uni10 can run with `CPU` only, with
`GPU`, and in the future, on an `MPI` environment. Therefore, we
provide a simple way to change the environment variables.

Normally, Uni10 codes come with default environment settings,
but you can override them with a `.uni10rc` file

Here we provide details on how to do this.

### Add `Uni10Create()` and `Uni10Destroy()`

First, to invoke customized configuration, add `Uni10Create()` at the beginning
and `Uni10Destroy()` at the end of the main function.

```
#include "uni10.hpp"

using namespace uni10;

int main(){

  Uni10Create();

  ...
  ...

  Uni10Destroy();

  return 0;
}
```


### Create a `.uni10rc` under the same directory of your executable.


#### `.uni10rc`

##### CPU VERSION

Only number of threads could be modified by the user using `threadnum`

```
threadnum = 4
```

By default, the uni10 apps are single-threaded if the uni10 library was built
single-threaded. If built with OpenMP, several functions are accelerated.
Currently,  `Uni10::Permute()` and `Uni10::Contraction()` are threaded.


##### GPU VERSION

There are five variables that could be changed by `.uni10rc` in the GPU version,

```
mode = 0
device = 0
blocksize_x = 32
blocksize_x = 32
blocksize_z = 1
```

###### `mode`

| Mode value  |   |
| -------------- |------------------|
|  `mode = 0`   | <font color="red"> CPU-ONLY <font>|
|  `mode = 1`   | <font color="red"> HYBRID <font>|
|  `mode = 2`   | <font color="red"> GPU-ONLY <font>|

Notice that `HYBRID` mode is not yet implemented
and `GPU-ONLY` mode provides support for double precision only.

###### `device`

To specify which GPU device to use if there are more than one cards on
the machine. Take the example that the output of `$ nvidia-smi` is,

```
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 375.51                 Driver Version: 375.51                    |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce GTX 980     Off  | 0000:01:00.0     Off |                  N/A |
|  0%   41C    P0    54W / 195W |      0MiB /  4036MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+
|   1  GeForce GTX 108...  Off  | 0000:02:00.0     Off |                  N/A |
|  0%   46C    P0    55W / 250W |      0MiB / 11172MiB |      2%      Default |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID  Type  Process name                               Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+

```
In this case, `device = 0` and `device = 1` means running uni10 app with
`GTX 980` and `GTX 1080`, respectively.


###### `blocksize_x`, `blocksize_y` and `blocksize_z`

Some linear solvers are not yet supported by NVIDIA *CUsolver*,
users can modify `blocksize` if the default values are not optimal.

##### Messages from the Developers

>This project, `Uni10 Configuration`, is still experimental.
Improving the environment management subsystem and
make Uni10 to interoperate smoothly on different platforms is under
development.
