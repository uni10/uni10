#ifndef __UNI10_HIGH_RANK_LINALG_H__
#define __UNI10_HIGH_RANK_LINALG_H__

#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_trans.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_dagger.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_permute.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_pseudoPermute.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_contract.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_pseudoContract.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_otimes.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_partialTrace.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_trace.h"
#include "uni10_api/uni10_hirnk_linalg/uni10_hirnk_linalg_conj.h"

#endif
